function calculator(a, operation, b) {
    switch (operation) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a / b;
        default:
            return console.log("that is not a valid operation");
    }
}

module.exports = calculator;