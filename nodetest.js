const events = require('events');
const os = require('os');

const eventEmitter = new events.EventEmitter();

const testString = 'i am the test event';
const CPUString = `Avg CPU Load: ${os.loadavg()}`;

function eventHandler(teststr) {
    console.log(teststr);
    eventEmitter.emit('test_event');
}

function cpuhandler(cpustr) {
    console.log(cpustr);
    eventEmitter.emit('cpu_event');
}


eventEmitter.on('test_event', () => {
    console.log('test event received.');
})

eventEmitter.on('cpu_event', () => {
    console.log('CPU event reached.')
})

eventHandler(testString, cpuhandler(CPUString));


/* let buffer = Buffer.alloc(10);
const fs = require('fs');

console.log(buffer);

console.log(fs.readFileSync("sample.txt"));
console.log(fs.readFileSync("sample.txt").toString('utf8'));*/

/*const os = require('os');

console.log(`CPU Uptime: ${os.uptime()}`);
console.log(`Avg CPU Load: ${os.loadavg()}`);
console.log('Info of CPU:' +
    JSON.stringify(os.cpus()));*/


/*const dns = require('dns');

dns.resolve4('google.com', (err, addresses) => {
    if (err) {
        throw err;
    } else {
        console.log('addresses: ' +
            JSON.stringify(addresses));
    }
})*/

/*const fs = require('fs');

const text = "haha this is magic idk what to do my head hurts";

fs.mkdir('test', () => {
    fs.writeFile('test/wow.txt', text, (err) =>
        fs.readFile('test/wow.txt', (err, data) => {
            console.log(data.toString('utf8'));
        }))
})*/

/*const fs = require('fs');

fs.readFile('./sample.txt', (err, data) => {
    //adsasd
    if (err) {
        console.error(err.message);
        throw err;
    }

    console.log(data.toString('utf8'));
})*/

/*const http = require('http');
const hostname = 'localhost';
const port = 3000;

const server = http.createServer((req, res) => {
    console.log(req.headers);

    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.write('hello achievers');
    res.end('<h1>hello world</h1>');
})
server.listen(port, hostname, () => {
    console.log(`check http://${hostname}:${port}/`);
}) */

//console.log(process.argv)
/*console.log(__dirname)

console.log(__filename)
setTimeout(function () {
    console.log("ready set go");
}, 3000)

console.log(process.cwd());*/