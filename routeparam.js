const koa = require('koa');
const Router = require('koa-router');
const app = new koa();
const router = new Router();
const port = 8080;

/*router.get('/test/:inside', (ctx) => {
    const inside = ctx.params.inside;
    const out = `this is what you wrote: ${inside}`;
    ctx.status = 200;
    ctx.body = out;
})*/

router.get('/test/:inside([0-9]{5})', ctx => {
    try {
        const inside = ctx.params.inside;
        console.log(`Router reached. Output of inside: ${inside}`);
        const out = `this is what you wrote: ${inside}`;
        ctx.status = 200;
        ctx.body = out;
    }
    catch (err) {
        console.log(`caught error at: ${err}`)
    }

})

router.all(/.*/, function (ctx) {
    ctx.app.emit('notfound')
    console.log("reached not found error");
})

app.on('notfound', ctx => {
    if (process.env.NODE_ENV != 'test') {
        console.log("Entry not found or not allowed.");
    }
})

app.use(router.routes());
app.listen(port);
console.log(`go to http://localhost:${port}/test/`);