const fs = require('fs');

const readerStream = fs.createReadStream('out.txt');

const writerStream = fs.createWriteStream('out2.txt');

readerStream.pipe(writerStream);

console.log('end')

/*const fs = require('fs');

let data = ''

const readerStream = fs.createReadStream('out.txt');

readerStream.setEncoding('utf8');

readerStream.on('data', (chunk) => {
    data += chunk;
})

readerStream.on('end', () => {
    console.log(data);
})

readerStream.on('error', (err) => {
    console.error(err.stack);
})

console.log('end')*/

/*const fs = require('fs');
const { writer } = require('repl');

let data = 'My brain is streaming this info right now';

const writerStream = fs.createWriteStream('out.txt');

writerStream.write(data, 'utf8');

writerStream.end();

writerStream.on('finish', () => {
    console.log('written to disk');
})

writerStream.on('error', (err) => {
    console.error(err.stack);
})

console.log('end.')*/