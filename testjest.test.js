const calc = require('./testjest')

test('1 + 4 = 5', () => {
    expect(calc(1, '+', 4)).toBe(5);
})

test('4 - 1 = 3', () => {
    expect(calc(4, '-', 1)).toBe(3);
})

test('2 * 4 = 8', () => {
    expect(calc(2, '*', 4)).toBe(8);
})

test('4 / 4 = 1', () => {
    expect(calc(4, '/', 4)).toBe(1);
})

test('Handle unknown operator', () => {
    expect(calc(4, '=', 4)).toBe(console.log("that is not a valid operation"));
})

test('Handle unknown operator of not type char', () => {
    expect(calc(4, 5, 4)).toBe(console.log("that is not a valid operation"));
})