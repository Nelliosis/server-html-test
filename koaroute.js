const koa = require('koa');
const Router = require('koa-router');

const app = new koa();
const router = new Router();

router.get('/hello', (ctx) => {
    const data = `hi world`;
    ctx.status = 200;
    ctx.body = data;
})

router.all('/*', function (ctx, next) {
    console.log("accessing something special");
    next();
})
app.use(router.routes());
app.listen(8080);
console.log("server started at port 8080")